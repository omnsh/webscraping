# Atelier webscraping OMNSH

Bienvenue sur la page de notre atelier de moissonnage du web !

Cet atelier, donné le 1er décembre 2018 à Paris pour les membres de l'OMNSH par Gabriel Alcaras et Yannick Rochat, est divisé en deux parties que vous trouverez dans le dossier [support](https://gitlab.com/omnsh/webscraping/tree/master/supports).

## Comment utiliser ces ressources ?

La première partie, celle de Gabriel, est disponible [ici](https://gitlab.com/omnsh/webscraping/blob/master/supports/01_webscraping_101.md). C'est du markdown, donc ça s'affiche proprement dans votre navigateur.

La seconde partie, celle de Yannick, est disponible [ici](https://gitlab.com/omnsh/webscraping/blob/master/supports/02_webscraping_101.md). Elle s'affiche également agréablement dans le navigateur.

Cette seconde partie contient des lignes de code, auxquelles vous pouvez accéder directement via un [*notebook*](https://gitlab.com/omnsh/webscraping/blob/master/supports/02_webscraping_101.Rmd). Nous vous recommandons de le télécharger (en fait de télécharger tout le dossier) et de l'ouvrir dans RStudio.

Vous devez avoir installé [R](https://cran.univ-paris1.fr/) et [RStudio](https://www.rstudio.com/products/rstudio/download/#download) pour ouvrir le notebook contenant le code (le fichier se terminant par l'extension .Rmd) et exécuter celui-ci.

Les documents contiennent volontairement beaucoup de texte, afin que les personnes présentes puissent s'y référer même après le cours.

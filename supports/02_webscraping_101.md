Introduction
============

------------------------------------------------------------------------

Dans cette seconde partie du cours, nous apprendrons comment utiliser ce
qui a été vu avec le langage de programmation R.

Ainsi, nous allons commencer par une *rapide* introduction à R, puis
nous verrons:

-   Comment identifier dans le code les éléments qui nous intéressent
    (les «nodes»).
-   Comment les enregistrer/télécharger («scraper»).
-   Comment automatiser ceci sur une plus large échelle («crawler»).

R/RStudio
=========

Qu'est-ce que R ?
-----------------

-   R est un langage de programmation ainsi qu'un logiciel prévu à
    l'origine pour faire de l'analyse statistique sur Mac sur la base du
    langage propriétaire S.
-   Il a rapidement connu du succès, jusqu'à devenir un langage de
    référence aujourd'hui.
-   R est particulièrement apprécié en sciences humaines et sociales.
-   La communauté R est
    [bienveillante](https://hackernoon.com/which-programming-languages-have-the-happiest-and-angriest-commenters-ebe91b3852ed#.hhwduwm45)…

------------------------------------------------------------------------

<img src="imgs/stack.png" width="800px" />

------------------------------------------------------------------------

<img src="imgs/stack2.png" width="800px" />

Qu'est-ce que R ?
-----------------

-   R est un langage de programmation. Il peut par exemple être lancé
    dans un terminal.
-   R désigne également une interface de programmation que l'on nomme R
    GUI, "GUI" signifiant "Graphical User Interface".
-   R GUI est un logiciel libre.

Qu'est-ce que RStudio ?
-----------------------

-   RStudio est un environnement permettant de programmer en R,
    c'est-à-dire un peu plus qu'une simple interface graphique.
-   Le logiciel propose une console (en bas à gauche, là où le code est
    exécuté) et une fenêtre de script (en haut à gauche, là où l'on
    rédige le code que l'on veut réutiliser)
-   RStudio permet par exemple de connaître en tout temps les variables
    enregistrées en mémoire (la fenêtre en haut à droite), ou encore
    d'utiliser un notebook tel que celui qui a permis de générer ces
    slides.

Des ressources
--------------

**Introduction à R et au tidyverse** de Julien Barnier

<https://juba.github.io/tidyverse>

**R for Data Science** de Garrett Grolemund & Hadley Wickham

<http://r4ds.had.co.nz/>

**Les cheatsheets de RStudio**

<https://www.rstudio.com/resources/cheatsheets/>

Qu'est-ce qu'un package ?
-------------------------

Un package est un ensemble de fonctions réunies autour d'une thématique
commune.

------------------------------------------------------------------------

Un package doit tout d'abord être installé avec la fonction
`install.packages` avant d'être chargé avec la fonction `library`.

Un package doit être installé pour pouvoir être chargé. Il n'y a besoin
de faire cette étape qu'une fois, à moins d'une mise à jour du package.

Un package que l'on souhaite utiliser doit être chargé à chaque
utilisation de R.

Un certain nombre de packages sont installés et chargés d'office lorsque
R est lancé pour la première fois.

------------------------------------------------------------------------

La fonction ci-dessous liste toutes les fonctions du package `rvest` (un
package que nous allons utiliser dans ce cours), pour donner un exemple.

    library(rvest)
    help(package="rvest")

Si ceci vous donne un message d'erreur, c'est que le package n'a pas été
installé. Si besoin, voici un [guide
vidéo](https://www.youtube.com/watch?v=ljdfqMfWn_A) pour vous aider.

------------------------------------------------------------------------

On peut explorer les packages par exemple à travers les *task views*, où
ils sont regroupés par thématiques.

<https://cran.r-project.org/web/views/>

------------------------------------------------------------------------

La gestion des packages peut se faire avec l'onglet "Packages" dans la
fenêtre en bas à droite de RStudio.

Par exemple, dans ce cours, nous allons utiliser le package `rvest` et
le package `tidyverse`.

S'ils apparaîssent dans la liste des packages, c'est qu'ils sont
installés.

Lorsqu'ils sont cochés, celà signifie en plus qu'ils sont chargés dans
la session en cours.

Démonstrations
==============

------------------------------------------------------------------------

Le package `rvest` propose quelques démonstrations.

On peut les visualiser en tapant `demo()` dans la console.

Il y en a trois:

-   tripadvisor
-   united
-   zillow

et ce sont des ressources précieuses.

------------------------------------------------------------------------

Voici la commande qui permet de lancer une de ces démonstrations.

    demo(tripadvisor)

Quelques exemples
=================

Les premières opérations
------------------------

    ## Une simple addition
    4 + 3         

    ## [1] 7

    ## Priorité des opérations
    2^2 + 3*4     

    ## [1] 16

Sauvegarde d'une valeur dans une variable
-----------------------------------------

    z <- 12.5
    z

    ## [1] 12.5

Addition de deux variables
--------------------------

    x <- 12.5
    y <- 7.5
    x + y

    ## [1] 20

Chaînes de caractères
---------------------

Les chaînes de caractères («*character*») sont des assemblages de
symboles.

    a <- "introduction"
    str(a)

    ##  chr "introduction"

Tableau de données
------------------

Les tableaux de données sont un type d'objet parmi les plus utilisés sur
R.

Un tableau de données -- un *data frame* -- contient en colonnes des
variables pouvant être de plusieurs types (numériques, ordinales,
catégorielles, logique, etc.) et en ligne les observations.

------------------------------------------------------------------------

Par exemple,

    df <- data.frame(age = c(10, 11, 9), 
            nom = c("Béatrice", "Camille", "Arthur"), 
            taille = factor(c("grand", "petit", "moyen"), 
             levels = c("petit", "moyen", "grand")),
            test = c(TRUE, TRUE, FALSE))
    df

    ##   age      nom taille  test
    ## 1  10 Béatrice  grand  TRUE
    ## 2  11  Camille  petit  TRUE
    ## 3   9   Arthur  moyen FALSE

------------------------------------------------------------------------

On en explore la structure avec la fonction `str`.

    str(df)

    ## 'data.frame':    3 obs. of  4 variables:
    ##  $ age   : num  10 11 9
    ##  $ nom   : Factor w/ 3 levels "Arthur","Béatrice",..: 2 3 1
    ##  $ taille: Factor w/ 3 levels "petit","moyen",..: 3 1 2
    ##  $ test  : logi  TRUE TRUE FALSE

------------------------------------------------------------------------

Si on ne travaille pas avec des données ordinales, il est recommandé de
désactiver la reconnaissance des variables textuelles comme variables
factorielles («*factor*») au sens où l'entend R.

    df <- data.frame(age = c(10, 11, 9), 
            nom = c("Béatrice", "Camille", "Arthur"), 
            taille = factor(c("grand", "petit", "moyen"), 
             levels = c("petit", "moyen", "grand")),
            test = c(TRUE, TRUE, FALSE),
            stringsAsFactors = FALSE)
    df

    ##   age      nom taille  test
    ## 1  10 Béatrice  grand  TRUE
    ## 2  11  Camille  petit  TRUE
    ## 3   9   Arthur  moyen FALSE

------------------------------------------------------------------------

Cela se fait grâce à l'option `stringsAsFactors = FALSE` dans le chunk
précédent.

    str(df)

    ## 'data.frame':    3 obs. of  4 variables:
    ##  $ age   : num  10 11 9
    ##  $ nom   : chr  "Béatrice" "Camille" "Arthur"
    ##  $ taille: Factor w/ 3 levels "petit","moyen",..: 3 1 2
    ##  $ test  : logi  TRUE TRUE FALSE

------------------------------------------------------------------------

Cette option (à désactiver) est importante à connaître lorsqu'on
travaille avec des chaînes de caractères.

Créer une fonction
------------------

De nombreuses fonctions sont préenregistrées dans R.

Par exemple, `nchar` permet de calculer la longueur d'une chaîne de
caractères (le nombre de symboles), et `runif` de faire des tirages
selon une loi uniforme.

    nchar("hello")

    ## [1] 5

    runif(10)

    ##  [1] 0.47093341 0.93104336 0.17201945 0.50432883 0.53654775 0.40611020
    ##  [7] 0.37598129 0.58311193 0.47144861 0.04822502

------------------------------------------------------------------------

Nous allons également voir comment créer une fonction.

Cela se fait en choisissant un nom (ici `write_that_word`), en
définissant un certain nombre de variables en entrée («*input*»), et en
décrivant le comportement de la fonction («*output*»), par exemple:

    write_that_word <- function(x) {cat(x)}

    ## 'cat' est une fonction qui écrit le mot qu'on lui donne. 

    write_that_word("hello")

    ## hello

------------------------------------------------------------------------

Voici un autre exemple de fonction, avec cette fois deux variables au
lieu d'une.

    add_these_numbers <- function(x, y) {x + y}

    add_these_numbers(14, 91)

    ## [1] 105

------------------------------------------------------------------------

Une fonction peut faire plus d'une ligne et exécuter du code un peu plus
compliqué que ce que nous avons vu jusque là.

    add_these_numbers2 <- function(x, y) {
      res <- x + y
      cat(paste("L'addition de", x, "et", y, "donne", res))
    }

    add_these_numbers2(14, 91)

    ## L'addition de 14 et 91 donne 105

------------------------------------------------------------------------

Il est même possible d'utiliser le résultat d'une fonction à l'intérieur
d'une fonction.

    add_these_numbers2(add_these_numbers(14, 91), 1)

    ## L'addition de 105 et 1 donne 106

------------------------------------------------------------------------

-   Nous avons ((très) rapidement) vu les bases de R.
-   N'hésitez pas à utiliser les ressources citées précédemment -- elles
    contiennent des exemples -- ou d'autres ressources de votre choix
    pour apprendre à maîtriser R et RStudio.

Obtenir de l'aide…
------------------

… quand on connaît le nom de la fonction.

    ## La page d'aide d'une variable
    help(exp)                     

    ## Donne le même résultat :
    ?exp                          

La touche de tabulation peut être très utile ici lorsqu'on connaît les
premières lettres d'une fonction.

Obtenir de l'aide…
------------------

… quand on ne connaît pas le nom de la fonction mais que l'on connaît
celui de la méthode.

    ## Chercher dans les pages d'aide
    help.search("linear model")   

    ## Donne le même résultat :
    ??"linear model"   

------------------------------------------------------------------------

Dans ce notebook comme dans les scripts que vous trouverez en ligne, les
fonctions sortent du chapeau comme si de rien n'était.

Il ne faut pas se faire d'illusions:

-   Il a d'abord fallu découvrir leur existence.
-   Puis comprendre leur fonctionnement.
-   Le plus souvent en faisant des erreurs au passage.

------------------------------------------------------------------------

C'est pour cette raison qu'il ne faut **jamais** hésiter à faire une
recherche

-   dans les pages d'aide de R
-   avec `?` lorsqu'on connaît la fonction.
-   avec `??` lorsqu'on ne la connaît pas.
-   en ligne
-   sur stackoverflow (par exemple).
-   sur un moteur de recherche.

Veille en ligne
---------------

-   Avec Twitter et le hashtag \#Rstats
-   Sur R-blogger, un agrégateur de blogs dont le thème est le langage R
    <https://www.r-bloggers.com/>
-   Sur les mailing lists <https://www.r-project.org/mail.html>

Scraper
=======

Au programme
------------

1.  Récupérer un tableau de données
2.  Concevoir un *web scraper*
3.  Concevoir un *web crawler*

Extraire un tableau
-------------------

Julia Silge @juliasilge écrit le 12 janvier sur Twitter :

> Somebody asked me for help with this, and I now know that you can
> scrape Wikipedia for the members of the House of Representatives in
> \#rstats code that is less than a tweet:

------------------------------------------------------------------------

<img src="imgs/silge.png" width="800px" />

Source : <https://twitter.com/juliasilge/status/951639629182074880>

------------------------------------------------------------------------

<img src="imgs/silge_wiki.png" width="800px" />

------------------------------------------------------------------------

<img src="imgs/silge_twitter.png" width="800px" />

------------------------------------------------------------------------

Pour faire tourner ce code aujourd'hui, on peut faire quelques
ajustements.

    library(rvest)
    library(tidyverse)

    h <- read_html("https://en.wikipedia.org/wiki/Current_members_of_the_United_States_House_of_Representatives")

    reps <- h %>%
      html_node("#votingmembers") %>%
      html_table(fill = TRUE)

    reps <- reps[,c(1:2,4:9)] %>%
      as_tibble()

%&gt;%
------

Les opérateurs qu'elle utilise entre les fonctions -- c'est-à-dire
`%>%`, que l'on nomme "pipe" -- permettent de prendre le résultat en
sortie d'une fonction («*output*») et de le donner en entrée («*input*»)
à la fonction suivante.

Pour plus d'information sur ce sujet, la ligne suivante permet
d'atteindre le fichier d'aide dans RStudio.

    help(topic = "%>%", package = magrittr)

Ressources
----------

Avant de poursuivre, voici quelques ressources utiles pour faire du *web
scraping* avec R.

-   <https://blog.rstudio.com/2014/11/24/rvest-easy-web-scraping-with-r/>
-   <https://thinkr.fr/rvest/>
-   <http://edutechwiki.unige.ch/fr/Web_scraping_avec_R>
-   <https://www.datacamp.com/community/tutorials/r-web-scraping-rvest>
-   <https://github.com/yusuzech/r-web-scraping-cheat-sheet> (incomplet)
-   <https://www.youtube.com/watch?v=gSbuwYdNYLM>

Télécharger une page entière
----------------------------

Approche basique: il est possible de télécharger une page entière avec
la fonction `readLines` par exemple, mais c'est inefficace. `readLines`
va sauver la page html entière sans extraire le contenu de manière
structurée.

------------------------------------------------------------------------

À ce propos, utiliser des expressions régulières (pour celles et ceux
qui les connaissent) est en général
[déconseillé](https://stackoverflow.com/questions/1732348/regex-match-open-tags-except-xhtml-self-contained-tags)
pour scraper des pages web.

------------------------------------------------------------------------

<img src="imgs/html_regex.png" width="500px" />

------------------------------------------------------------------------

Au programme
------------

Nous allons prendre doctissimo.fr et jeuxvideo.com comme exemples pour
expérimenter deux concepts fondamentaux du *web scraping* :

-   Extraire les posts d'un sujet de forum qui tient sur une page.
    (*scraper*)
-   Extraire les posts d'un sujet de forum qui tient sur plusieurs
    pages. (*scraper* + *crawler*)

Développer un scraper
---------------------

Prenons le sujet suivant, [« Je me
relance... »](http://forum.doctissimo.fr/sante/arreter-fumer/relance-sujet_189942_1.htm),
posté par puce410 le 22 octobre 2018 sur le site doctissimo.fr

La quinzaine de messages qu'il contient tiennent sur une seule page,
c'est-à-dire qu'il n'est pas nécessaire de cliquer sur un lien/bouton
« suivant » pour afficher toute la conversation. Nous n'avons pas besoin
ici d'un *crawler*, juste d'un *scraper*.

------------------------------------------------------------------------

Pour commencer, nous devons identifier quelles sont les informations que
nous souhaitons récupérer. En effet, tout sera sauvé dans un tableau de
données (ou feuille Excel, feuille de calcul, …) afin de permettre de
passer ensuite à l'analyse, dans R ou dans un autre logiciel (par ex.
Excel ou LibreOffice).

------------------------------------------------------------------------

Pour l'exemple qui nous occupe, nous souhaitons extraire les
informations suivantes :

-   Le nom de l'auteur-e du post.
-   La date du post.
-   Le texte du post.

Nous devons identifier où se trouvent ces trois éléments dans la page
HTML.

S'il est facile de les identifier pour un seul message, il faut
toutefois s'assurer de les avoir inclus pour tous les posts de la page
et de ne pas avoir inclus des informations qui ne nous intéressent pas.

Nous allons également avoir besoin du package `rvest`.

« SelectorGadget »
------------------

Cet outil permet de connaître le sélecteur CSS d'un élément précis dans
la page, mais également d'isoler un chemin générique qui permet de
récupérer le même élément répété à plusieurs reprises sur la page.

Vous pouvez le trouver ici <https://selectorgadget.com/> Merci de
l'installer avant de continuer.

L'utilisation la plus simple est de le placer dans la barre des signets
(vérifié avec Firefox.)

Qu'est-ce que rvest ?
---------------------

> rvest helps you scrape information from web pages. It is designed to
> work with `magrittr` to make it easy to express common web scraping
> tasks, inspired by libraries like beautiful soup.

<https://github.com/hadley/rvest>

C'est un package intégré à l'ensemble de packages `tidyverse` (voir le
[**tidy tools
manifesto**](https://cran.r-project.org/web/packages/tidyverse/vignettes/manifesto.html)
pour comprendre de quoi il s'agit).

Charger le package et extraire la page
--------------------------------------

`read_html` télécharge la page sur laquelle on travaille.

Dans le code ci-dessous, cette information est enregistrée dans l'objet
`doctissimo`.

    library(rvest)

    doctissimo <- read_html("http://forum.doctissimo.fr/sante/arreter-fumer/relance-sujet_189942_1.htm")

    ## Si l'URL ne s'affiche pas entièrement, vous pouvez la retrouver dans le notebook (le fichier avec une extension .Rmd).

------------------------------------------------------------------------

    ## La fonction suivante affiche toute la page, brute

    html_text(doctissimo)

    ## Ce qui n'est pas très utile.

Extraire les noms
-----------------

Pour pouvoir extraire les noms, il faut connaître leur emplacement dans
la page html.

Pour connaître leur emplacement, nous utilisons SelectorGadget.

SelectorGadget demande parfois un peu de dextérité. On sélectionne
quelques éléments que l'on souhaite garder et il tente de deviner à
partir de là quels sont tous les éléments que nous cherchons à isoler.
Pour l'aider dans cette tâche, on désélectionne les éléments non
souhaités, on resélectionne d'autres éléments, etc.

------------------------------------------------------------------------

<img src="imgs/autovrac3.png" width="800px" />

------------------------------------------------------------------------

Ce que l'on obtient, l'expression `.s2 .CF_user_mention`, est un
sélecteur CSS.

En l'indiquant à la fonction `html_nodes`, nous pouvons extraire les
éléments de la page qui correspondent à ce sélecteur.

    doctissimo_noms <- html_nodes(doctissimo, ".s2 .CF_user_mention")
    length(doctissimo_noms)

    ## [1] 18

La fonction `length` nous dit combien d'objets ont été retenus (ici 18).
Cela correspond au résultat qu'affichait SelectorGadget dans l'image
précédente.

------------------------------------------------------------------------

En utilisant ensuite la fonction `html_text`, on demande à `rvest` de
nous sélectionner uniquement le texte de ces éléments.

    doctissimo_noms <- html_text(doctissimo_noms)
    doctissimo_noms

    ##  [1] "puce410"          "puce410"          "puce410"         
    ##  [4] "piero39"          "puce410"          "autovrac3"       
    ##  [7] "Lamissalex​iadu57" "puce410"          "autovrac3"       
    ## [10] "Lamissalex​iadu57" "puce410"          "pews"            
    ## [13] "autovrac3"        "puce410"          "autovrac3"       
    ## [16] "puce410"          "pews"             "James"

Extraire les dates
------------------

On procède de même (`html_nodes` puis `html_text`) pour extraire les
dates.

    doctissimo_heure <- html_nodes(doctissimo, ".topic_posted")
    doctissimo_heure <- html_text(doctissimo_heure)
    doctissimo_heure

    ##  [1] "Posté le 22/10/2018 à 20:54:43  " "Posté le 22/10/2018 à 21:10:20  "
    ##  [3] "Posté le 22/10/2018 à 21:10:21  " "Posté le 23/10/2018 à 05:42:58  "
    ##  [5] "Posté le 23/10/2018 à 06:26:45  " "Posté le 23/10/2018 à 12:32:11  "
    ##  [7] "Posté le 23/10/2018 à 16:05:32  " "Posté le 23/10/2018 à 16:19:59  "
    ##  [9] "Posté le 25/10/2018 à 10:38:56  " "Posté le 25/10/2018 à 11:53:26  "
    ## [11] "Posté le 25/10/2018 à 16:03:47  " "Posté le 27/10/2018 à 03:56:15  "
    ## [13] "Posté le 30/10/2018 à 18:59:45  " "Posté le 31/10/2018 à 10:32:50  "
    ## [15] "Posté le 01/11/2018 à 19:54:26  " "Posté le 01/11/2018 à 22:50:02  "
    ## [17] "Posté le 02/11/2018 à 11:23:31  " "Posté le 15/11/2018 à 10:10:46  "

    length(doctissimo_heure)

    ## [1] 18

------------------------------------------------------------------------

Dans ce cas précis, les informations ne sont pas encore lisibles par R à
ce stade. Il (le langage) ou elle (l'application) considère qu'il s'agit
de chaînes de caractères et ne comprend pas que ce sont des dates.

Il est nécessaire de faire quelques manipulations (avec le package
`stringr`), puis de faire reconnaître les données comme des dates (avec
le package `lubridate`).

------------------------------------------------------------------------

`stringr` est un package conçu pour la manipulation de chaînes de
caractères. Il possède [sa propre
cheatsheet](https://www.rstudio.com/resources/cheatsheets/#stringr).

Nous pouvons par exemple retirer les mots "Posté le ".

Note: le symbole `\\s` désigne un espace (cf. la cheatsheet).

    library(stringr)
    doctissimo_heure <- str_remove_all(doctissimo_heure, "Posté le\\s")
    doctissimo_heure

    ##  [1] "22/10/2018 à 20:54:43  " "22/10/2018 à 21:10:20  "
    ##  [3] "22/10/2018 à 21:10:21  " "23/10/2018 à 05:42:58  "
    ##  [5] "23/10/2018 à 06:26:45  " "23/10/2018 à 12:32:11  "
    ##  [7] "23/10/2018 à 16:05:32  " "23/10/2018 à 16:19:59  "
    ##  [9] "25/10/2018 à 10:38:56  " "25/10/2018 à 11:53:26  "
    ## [11] "25/10/2018 à 16:03:47  " "27/10/2018 à 03:56:15  "
    ## [13] "30/10/2018 à 18:59:45  " "31/10/2018 à 10:32:50  "
    ## [15] "01/11/2018 à 19:54:26  " "01/11/2018 à 22:50:02  "
    ## [17] "02/11/2018 à 11:23:31  " "15/11/2018 à 10:10:46  "

------------------------------------------------------------------------

Le package `lubridate` est conçu pour gérer les dates. Il possède [sa
propre
cheatsheet](https://www.rstudio.com/resources/cheatsheets/#lubridate).

On y trouve la fonction `dmy_hms`, qui correspond au format de date de
nos données.

------------------------------------------------------------------------

Nous supprimons le "à" et laissons seulement un espace.

    doctissimo_heure <- str_remove_all(doctissimo_heure, "à\\s")
    doctissimo_heure

    ##  [1] "22/10/2018 20:54:43  " "22/10/2018 21:10:20  "
    ##  [3] "22/10/2018 21:10:21  " "23/10/2018 05:42:58  "
    ##  [5] "23/10/2018 06:26:45  " "23/10/2018 12:32:11  "
    ##  [7] "23/10/2018 16:05:32  " "23/10/2018 16:19:59  "
    ##  [9] "25/10/2018 10:38:56  " "25/10/2018 11:53:26  "
    ## [11] "25/10/2018 16:03:47  " "27/10/2018 03:56:15  "
    ## [13] "30/10/2018 18:59:45  " "31/10/2018 10:32:50  "
    ## [15] "01/11/2018 19:54:26  " "01/11/2018 22:50:02  "
    ## [17] "02/11/2018 11:23:31  " "15/11/2018 10:10:46  "

------------------------------------------------------------------------

Les données sont maintenant au bon format pour être lues par la fonction
`dmy_hms`.

    library(lubridate)
    doctissimo_heure <- dmy_hms(doctissimo_heure)
    str(doctissimo_heure)

    ##  POSIXct[1:18], format: "2018-10-22 20:54:43" "2018-10-22 21:10:20" ...

------------------------------------------------------------------------

`POSIXct` est un format de date. Cela signifie que notre opération a
fonctionné.

Extraire les textes
-------------------

On extrait les textes des messages postés.

    doctissimo_texte <- html_nodes(doctissimo, ".post_content div")
    doctissimo_texte <- html_text(doctissimo_texte)

------------------------------------------------------------------------

La fonction `head` donne les 6 premiers éléments d'une liste d'objets.

On voit apparaître une série d'artefacts… le site doctissimo.fr n'est
*pas exactement* un bon modèle de web design et SelectorGadget est à la
peine ici.

    head(doctissimo_texte)

    ## [1] "CoucouCertains me connaissent ici car j'y suis passée plusieurs pour plusieurs tentatives d'arrêt.J'ai bien essayé 5-6 fois en 1 an mais je rechute...Pour l'instant je supporte très mal la cigarette. Je me suis chopé une œsophagite et gastrite il y a une semaine. J'ai du mal à m'en débarrasser. Et j'ai lu que la cigarette aggravait tout.Je m'étais fixé la date du 5 novembre. Le lendemain d'un gros tournoi où la cigarette aurait pu me faire destresser.. Mais comme je ne suis pas en forme en ce moment, je me fixe ce jeudi 25 avril à 13h30  Pourquoi jeudi et pas ce soir ou demain ou mercredi me direz-vous ? Car, comme ceux qui se souviennent de moi, savent, le mercredi est mon off, une matinée à moi toute seule où je profite pour fumer mes cigarettes tranquillement...Je n'ai pas beaucoup de dépendant physique au final. Parfois je peux tenir 5 heures sans m'en soucier. Le pire est donc la dépendance psychologique. J'ai mes heures, j'ai mes habitudes, le moment.C'est très très dur de se priver de ses habitudes.J'ai été voir une psychologue en tabacologie qui m'a dit que j'étais trop stressee en ce moment pour arrêter maintenant. Mais en fait je realise que me vie est un stress permanent et que ce ne sera jamais le bon moment.Très peu de soutien du côté du mari qui quand je lui dis que j'ai trop envie de fumer, me répond : ah ben moi je vais aller m'en fumer une....Grrr merci quoi....Et qui laisse ses paquets et son odeur partout...Quand je fume, ça me dégoute de plus en plus. Mais à chaque fois que c'est le moment de fumer la dernière, c'est là que je l'apprécie, comme par hasard. Alors je fume clope sur clope sur clope. Je n'arrive pas à me faire à l'idée de la dernière.... Une fois il m'en restait 7 dans le paquet. J'ai tout fumé d'un coup.... Et j'ai repris même pas 2 jours plus tard.Qui aurait des conseils pour m'aider à passer cette épreuve ?Je crois que j'ai installé toutes les applications possibles et imaginables... Mais le souci c'est qu'après que le plus gros soit fait, les curseurs ne bougent plus avant x jours. Donc ça me démotive....Un compteur est resté allumé depuis ma dernière tentative en juillet. Si j'avais continué à arrêter, j'aurais pu économiser quasi 500 jro ---------------\r\n\t\t\t  "
    ## [2] ""                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    ## [3] ""                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    ## [4] "Oups plus de batterie...Déjà je rectifie... Je me fixe la date du 25 octobre et pas 25 avril.Et comme je disais j'aurais pu économiser près de 500 euros.Quand je vois mon entourage se faire des belles vacances et pas nous, je me dis que si on rajoutait à notre budget les presque 4000 euros dépensés en fumée, nous aussi on irait sur des plages paradisiaques....Enfin voilà. J'espère trouver du soutien ici. J'avoue que la fois où j'ai tenu le plus longtemps (49,5 jours) c'était grâce au forum Je compte sur vous pour m'aider !!!!Merci à tous ! ---------------\r\n\t\t\t  "                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
    ## [5] ""                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    ## [6] ""

------------------------------------------------------------------------

Notez le nombre d'observations (64) qui ne correspond pas avec les
nombres que nous avons trouvés précédemment (18).

    length(doctissimo_texte)

    ## [1] 64

------------------------------------------------------------------------

Ce nombre (64) apparaît également au moment de définir le sélecteur avec
SelectorGadget.

<img src="imgs/clear_64.png" width="800px" />

------------------------------------------------------------------------

Normalement, les ajustements se font en relançant l'expérience de
SelectorGadget, ou en allant directement lire et comprendre le code
source de la page Web.

Ici, le site de doctissimo est compliqué à utiliser.

Les ajustements peuvent être faits à la main, c'est-à-dire en
identifiant les lignes problématiques et en les retirant de l'ensemble.

Nous pouvons par exemple sauver le fichier, relever lesdites lignes,
puis revenir les retirer dans RStudio.

------------------------------------------------------------------------

Dans ce but, on sauve ces outputs sous forme d'un fichier que nous irons
consulter en dehors de RStudio.

    write.table(x = doctissimo_texte, 
                file = "doctissimo_text.txt", 
                col.names = FALSE)

------------------------------------------------------------------------

On choisit les lignes que l'on souhaite garder en les plaçant dans un
vecteur (avec `c`), lui-même entre crochets `[ ]`, et séparés par des
virgules.

La fonction `c` est nécessaire pour indiquer qu'il y a plusieurs lignes
à considérer.

    doctissimo_texte_final <- 
      doctissimo_texte[c(1,4,7,12,15,18,21,28,33,36,43,46,48,51,54,57,60,62)]
    length(doctissimo_texte_final)

    ## [1] 18

------------------------------------------------------------------------

La fonction `data.frame` permet de créer un tableau de données à partir
de nos extractions.

Attention, il est nécessaire de faire une vérification à la main -- en
tout cas des pointages -- pour être certain-e que chaque nom, chaque
date et chaque texte correspondent.

    doctissimo_df <- data.frame(
      nom = doctissimo_noms,
      date = doctissimo_heure,
      message = doctissimo_texte_final,
      stringsAsFactors = FALSE
    )

------------------------------------------------------------------------

À quoi ressemble le résultat ?

On utilise cette fois la fonction `str`, qui fonctionne pour tout objet
de R, et qui décrit ici les variables du tableau de données.

-   `chr` signifie que les variables "nom" et "message" sont des "chaîne
    de caractères".
-   `POSIXct` signifie que la variable "date" contient des dates au
    format "année-mois-jour heures:minutes:secondes".

<!-- -->

    str(doctissimo_df)

    ## 'data.frame':    18 obs. of  3 variables:
    ##  $ nom    : chr  "puce410" "puce410" "puce410" "piero39" ...
    ##  $ date   : POSIXct, format: "2018-10-22 20:54:43" "2018-10-22 21:10:20" ...
    ##  $ message: chr  "CoucouCertains me connaissent ici car j'y suis passée plusieurs pour plusieurs tentatives d'arrêt.J'ai bien essayé 5-6 fois en "| __truncated__ "Oups plus de batterie...Déjà je rectifie... Je me fixe la date du 25 octobre et pas 25 avril.Et comme je disais j'aurais pu éco"| __truncated__ "Oups plus de batterie...Déjà je rectifie... Je me fixe la date du 25 octobre et pas 25 avril.Et comme je disais j'aurais pu éco"| __truncated__ "bonjour Puce,je te souhaite de réussir, ne pas baisser les bras, ça finit par être payant il n'y a plus beaucoup de monde, tu d"| __truncated__ ...

------------------------------------------------------------------------

Le site de Doctissimo n'étant pas très bien *designé*, ce premier
exemple était plutôt hardu !

Essayez de travailler sur Wikipedia, par exemple, pour comprendre
comment le web scraping devient plus facile lorsqu'un site est mieux
structuré.

À l'inverse, vous tomberez peut-être sur des sites encore plus complexes
à *moissonner*.

Développer un crawler
=====================

------------------------------------------------------------------------

Pour développer un *crawler* («robot de recherche»), nous devons
commencer par créer une session de travail avec la fonction
`html_session`.

L'un des objectifs de cette démarche est de conserver un historique des
pages visitées.

Puis nous appliquons le scraper, passons à la page suivante avec la
fonction `follow_link`, appliquons à nouveau le scraper, etc.

------------------------------------------------------------------------

Voici un exemple où nous nous baladons sur trois pages consécutives du
forum 15-18 de jeuxvideo.com

    jvcom <- html_session("http://www.jeuxvideo.com/forums/42-50-58260743-1-0-1-0-brainstormons-pour-creer-de-nouveaux-delires.htm")

    jvcom %>% 
      follow_link("2") %>% 
      follow_link("3") %>% 
      session_history

    ##   http://www.jeuxvideo.com/forums/42-50-58260743-1-0-1-0-brainstormons-pour-creer-de-nouveaux-delires.htm
    ##   http://www.jeuxvideo.com/forums/42-50-58260743-2-0-1-0-brainstormons-pour-creer-de-nouveaux-delires.htm
    ## - http://www.jeuxvideo.com/forums/42-50-58260743-3-0-1-0-brainstormons-pour-creer-de-nouveaux-delires.htm

------------------------------------------------------------------------

En regardant les trois URLs, on reconnaît les trois premières pages
("1-0-1-0", "2-0-1-0" et "3-0-1-0") d'un sujet de forum.

Comment fonctionnent `jump_to` et `follow_link` ?
-------------------------------------------------

<img src="imgs/follow_link.png" width="800px" />

La marche à suivre…
-------------------

… pour créer un crawler, il faut…

1.  Ouvrir une session.
2.  Déterminer comment explorer toutes les pages.

<!-- -->

1.  Passer d'une page à la suivante grâce aux éléments de la page ?
    -&gt; `follow_link`
2.  Générer directement l'URL ? -&gt; `jump_to`

<!-- -->

1.  Mettre en place le crawler.
2.  Y insérer le scraper.

------------------------------------------------------------------------

Dans le crawler qui va suivre, on repartira toujours d'une même page,
mais de nombreux cas il est plus pertinent de partir de la page active
pour rejoindre la page suivante.

------------------------------------------------------------------------

Tout d'abord, nous allons identifier combien de pages contient le sujet
de forum ci-dessous.

On peut le faire à l'oeil (c'est 27), mais faisons l'exercice avec du
code.

    jvcom <- html_session("http://www.jeuxvideo.com/forums/42-50-58260743-1-0-1-0-brainstormons-pour-creer-de-nouveaux-delires.htm")

    html_node(jvcom, css = "#forum-main-col > div.conteneur-messages-pagi > div:nth-child(1) > div.bloc-liste-num-page > span:nth-child(12) > a") %>% html_text()

    ## [1] "27"

------------------------------------------------------------------------

Nous allons donc devoir parcourir les pages 1 à 27 (état au 1er décembre
2018) de ce sujet de forum.

Il est utile ici de découvrir le comportement des `:` dans R. Ils
permettent d'énumérer tous les nombres entiers entre deux nombres
donnés.

    1:3

    ## [1] 1 2 3

    12:17

    ## [1] 12 13 14 15 16 17

------------------------------------------------------------------------

La fonction `as.numeric` permet de transformer une chaîne de caractères
en nombres, pour autant que les caractères concernés soient des
chiffres.

    pages <- 1:as.numeric(
      html_node(
        jvcom, css = "#forum-main-col > div.conteneur-messages-pagi > div:nth-child(1) > div.bloc-liste-num-page > span:nth-child(12) > a") %>% html_text())

    pages

    ##  [1]  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23
    ## [24] 24 25 26 27

------------------------------------------------------------------------

Ainsi, l'objet `page` nous done tous les numéros de page souhaités.

Passer d'une page à l'autre
---------------------------

Pour tester ce que nous avons mis en place jusque là, nous faisons un
proto-crawler qui donne le nom du premier auteur d'un post pour chaque
page. On stocke ces noms dans un objet, `titres`, qui est vide au
départ.

Dans l'exemple, `for` désigne une boucle. Cette boucle consiste à itérer
la variable `i` en la faisant parcourir des valeur allant de 1 à 27.
L'objectif est de parcourir toutes les pages de ce sujet de forum.

Les nouvelles fonctions sont expliquées après l'exemple.

------------------------------------------------------------------------

    jvcom <- html_session("http://www.jeuxvideo.com")
    first_usernames <- c()

    for (i in pages) {
      
      next_link <- str_c("forums/42-50-58260743-", i, "-0-1-0-brainstormons-pour-creer-de-nouveaux-delires.htm")
      jvcom <- jvcom %>% jump_to(next_link)
      
      first_usernames[i] <- jvcom %>% 
        html_node(".text-user") %>% 
        html_text()
      
      jvcom <- jvcom %>% back()
    }

------------------------------------------------------------------------

On affiche le résultat, pour vérifier.

    head(first_usernames)

    ## [1] "\n                            Hubert-SJWW\n                        "    
    ## [2] "\n                            Hubert-SJWW\n                        "    
    ## [3] "\n                            SenorAsgarios\n                        "  
    ## [4] "\n                            ParticuleDeDieu\n                        "
    ## [5] "\n                            ParticuleDeDieu\n                        "
    ## [6] "\n                            ParticuleDeDieu\n                        "

Les fonctions utilisées
-----------------------

-   `str_c` nous a permis de générer ces différents chemins.
-   `jump_to` nous a permis d'atteindre une page à l'intérieur du site
    via son *chemin*.
-   `html_node` a un comportement différent de `html_nodes`. Il ne
    renvoie que le premier résultat.
-   `back` nous a permis de revenir à chaque fois à la page précédente.

Concevoir le scraper
--------------------

Nous construisons maintenant le *scraper*. Nous souhaitons enregistrer
les noms, dates et textes de tous les messages d'une page.

On les réunit ensuite dans un tableau de données.

------------------------------------------------------------------------

    jvcom <- html_session("http://www.jeuxvideo.com")

    jvcom <- jvcom %>% 
      jump_to("/forums/42-50-58260743-1-0-1-0-brainstormons-pour-creer-de-nouveaux-delires.htm")

    username <- jvcom %>% 
      html_nodes(".text-user") %>% 
      html_text()

    date <- jvcom %>% 
      html_nodes(css = "#forum-main-col .lien-jv") %>% 
      html_text()
    date <- date[13:32]

    texte <- jvcom %>% 
      html_nodes(".txt-msg") %>% 
      html_text()

    jvcom_page_1 <- data.frame(username = username, 
                               date = date, 
                               texte = texte, 
                               stringsAsFactors = FALSE)

Un aperçu du résultat
---------------------

Il y a énormément de caractères inutiles !

    head(jvcom_page_1)

    ##                                                              username
    ## 1 \n                            Hubert-SJWW\n                        
    ## 2     \n                            -Azorp-\n                        
    ## 3 \n                            Hubert-SJWW\n                        
    ## 4     \n                            -Azorp-\n                        
    ## 5 \n                            erreur--410\n                        
    ## 6     \n                            -Azorp-\n                        
    ##                          date
    ## 1 24 novembre 2018 à 21:31:47
    ## 2 24 novembre 2018 à 21:33:28
    ## 3 24 novembre 2018 à 21:34:00
    ## 4 24 novembre 2018 à 21:35:33
    ## 5 24 novembre 2018 à 21:36:16
    ## 6 24 novembre 2018 à 21:37:42
    ##                                                                                                                                                                                                                                                                                                                                               texte
    ## 1                \n                                                    Le forum doit arreter de vivre dans le passé, on va relancer la hype autour de jvc.RECREONS DES DELIRES, je pense que la meilleure première étape est de faire une liste de mêmes à potentiel.\nGo donner votre avis vos idées\n                                            
    ## 2                                                                                                                                                                                                                               \n                                                    Délire tintin  \n                                            
    ## 3 \n                                                    Le                                                     24 novembre 2018 à 21:33:28                                                                         -Azorp-                         a écrit :\nDélire tintin  Tu vas voir on va bider \n                                            
    ## 4                                                                                                                                                                                                                          \n                                                    J'ai confiance quay \n                                            
    ## 5                                                                                                                                                                                                       \n                                                    tintin ça pue la merde ne faites pas ça\n                                            
    ## 6                                                                                                                                                                                                                  \n                                                    Propose une idée alors quai \n

------------------------------------------------------------------------

Les `\n` représentent des retours à la ligne.

La fonction `View(jvcom_page_1)` montre que le résultat est
satisfaisant.

------------------------------------------------------------------------

Résultat final: le *web crawler*
--------------------------------

On l'obtient en insérer le scraper que nous venons de développer dans le
proto-crawler développé précédemment.

    library(dplyr)

    ## 
    ## Attaching package: 'dplyr'

    ## The following objects are masked from 'package:lubridate':
    ## 
    ##     intersect, setdiff, union

    ## The following objects are masked from 'package:stats':
    ## 
    ##     filter, lag

    ## The following objects are masked from 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

    jvcom <- html_session("http://www.jeuxvideo.com")

    jvcom_all_pages <- list()

    for (i in pages) {
      
      next_link <- str_c("forums/42-50-58260743-", i, "-0-1-0-brainstormons-pour-creer-de-nouveaux-delires.htm")
      jvcom <- jvcom %>% jump_to(next_link)
      
      username <- jvcom %>% html_nodes(".text-user") %>% html_text()

      date <- jvcom %>% html_nodes(css = "#forum-main-col .lien-jv") %>% html_text()
      if (i != tail(pages,1)) {
        date <- date[13:32]
      } else {
        date <- date[1:length(username)]
      }

      texte <- jvcom %>% html_nodes(".txt-msg") %>% html_text()
      
      jvcom_all_pages[[i]] <- data.frame(username = username, date = date, texte = texte, stringsAsFactors = FALSE)
      
      jvcom <- jvcom %>% back()
    }

    resultat <- bind_rows(jvcom_all_pages)

Un aperçu du résultat
---------------------

    head(resultat)

    ##                                                              username
    ## 1 \n                            Hubert-SJWW\n                        
    ## 2     \n                            -Azorp-\n                        
    ## 3 \n                            Hubert-SJWW\n                        
    ## 4     \n                            -Azorp-\n                        
    ## 5 \n                            erreur--410\n                        
    ## 6     \n                            -Azorp-\n                        
    ##                          date
    ## 1 24 novembre 2018 à 21:31:47
    ## 2 24 novembre 2018 à 21:33:28
    ## 3 24 novembre 2018 à 21:34:00
    ## 4 24 novembre 2018 à 21:35:33
    ## 5 24 novembre 2018 à 21:36:16
    ## 6 24 novembre 2018 à 21:37:42
    ##                                                                                                                                                                                                                                                                                                                                               texte
    ## 1                \n                                                    Le forum doit arreter de vivre dans le passé, on va relancer la hype autour de jvc.RECREONS DES DELIRES, je pense que la meilleure première étape est de faire une liste de mêmes à potentiel.\nGo donner votre avis vos idées\n                                            
    ## 2                                                                                                                                                                                                                               \n                                                    Délire tintin  \n                                            
    ## 3 \n                                                    Le                                                     24 novembre 2018 à 21:33:28                                                                         -Azorp-                         a écrit :\nDélire tintin  Tu vas voir on va bider \n                                            
    ## 4                                                                                                                                                                                                                          \n                                                    J'ai confiance quay \n                                            
    ## 5                                                                                                                                                                                                       \n                                                    tintin ça pue la merde ne faites pas ça\n                                            
    ## 6                                                                                                                                                                                                                  \n                                                    Propose une idée alors quai \n

------------------------------------------------------------------------

    str(resultat)

    ## 'data.frame':    533 obs. of  3 variables:
    ##  $ username: chr  "\n                            Hubert-SJWW\n                        " "\n                            -Azorp-\n                        " "\n                            Hubert-SJWW\n                        " "\n                            -Azorp-\n                        " ...
    ##  $ date    : chr  "24 novembre 2018 à 21:31:47" "24 novembre 2018 à 21:33:28" "24 novembre 2018 à 21:34:00" "24 novembre 2018 à 21:35:33" ...
    ##  $ texte   : chr  "\n                                                    Le forum doit arreter de vivre dans le passé, on va relancer la hype auto"| __truncated__ "\n                                                    Délire tintin  \n                                            " "\n                                                    Le                                                     24 novembre 2018 à"| __truncated__ "\n                                                    J'ai confiance quay \n                                            " ...

------------------------------------------------------------------------

Nous avons récolté les 533 observations composant ce sujet de forum !

Voici comment les extraire.

    write.csv2(resultat, file = "data_frame_crawler.txt")

------------------------------------------------------------------------

Un peu de manipulation d'expressions régulières sera encore nécessaire
pour avoir des données propres.

Next step
---------

Rendre le code plus propre pour qu'il puisse télécharger toutes les
entrées d'un sujet de forum rien qu'à partir de l'URL de la première
page.

Créer une fonction qui fasse celà.

Pourvoir télécharger tous les sujets de forum d'un site.

Puis, l'étape suivante pour faire du moissonage: les **APIs** ! ;-)

------------------------------------------------------------------------

**Merci pour votre attention !**

<img src="imgs/regex_horror.png" width="800px" />

# Introduction au webscraping

Le but de cette introduction est de **présenter les principes du webscraping
indépendamment d'une implémentation précise** dans un langage de programmation.
Il existe beaucoup d'outils différents pour écrire un webscraper, mais les
principes sous-jacents restent les mêmes et sont relativement simples.

<!-- vim-markdown-toc GitLab -->

* [Présentation générale](#présentation-générale)
  * [Qu'est-ce que le webscraping ?](#quest-ce-que-le-webscraping)
  * [Quels usages détournés ?](#quels-usages-détournés)
  * [Quand faut-il faire du webscraping ?](#quand-faut-il-faire-du-webscraping)
* [Comment ça marche ?](#comment-ça-marche)
  * [Oui, mais comment ça marche ?](#oui-mais-comment-ça-marche)
  * [Quelles sont les grandes étapes ?](#quelles-sont-les-grandes-étapes)
  * [Notre projet : botissimo](#notre-projet-botissimo)
* [Décortiquer une page web](#décortiquer-une-page-web)
  * [Un outil d'exploration essentiel : la console web](#un-outil-dexploration-essentiel-la-console-web)
  * [Afficher une page web](#afficher-une-page-web)
  * [Manipuler une page web](#manipuler-une-page-web)
* [Comment sélectionner des éléments d'une page web ?](#comment-sélectionner-des-éléments-dune-page-web)
  * [Les balises HTML](#les-balises-html)
  * [Une arborescence de balises](#une-arborescence-de-balises)
  * [Les attributs](#les-attributs)
  * [Les classes CSS](#les-classes-css)
  * [Le but du jeu](#le-but-du-jeu)
* [Faire de notre scraper une fonction](#faire-de-notre-scraper-une-fonction)
* [Comment naviguer dans un site web ?](#comment-naviguer-dans-un-site-web)
  * [Comment repérer des liens sur une page ?](#comment-repérer-des-liens-sur-une-page)
* [Quels outils pour faire du webscraping ?](#quels-outils-pour-faire-du-webscraping)
* [Aller plus loin](#aller-plus-loin)

<!-- vim-markdown-toc -->

# Présentation générale

## Qu'est-ce que le webscraping ?

Le webscraping consiste à extraire des informations d'une page web pour en
faire un usage détourné.

Les informations publiées sur un site web servent un usage premier pour les
personnes qui les produisent : poser une question, discuter avec des ami·e·s,
partager un logiciel, archiver des photos, etc.

Or on aimerait parfois faire un usage différent de ces informations, qui n'est
pas permis par le site d'origine. Le webscraping est un des moyens d'en
faire un usage détourné.

## Quels usages détournés ?

+ Faire un moteur de recherche
+ Récolter des adresses emails pour du spam (ou [voler des mots de passe](https://github.com/search?o=desc&q=filename%3Asftp-config.json+password&ref=searchresults&s=indexed&type=Code&utf8=%E2%9C%93))
+ Recevoir un message quand une page web change
+ [Remporter automatiquement des enchères](https://www.youtube.com/watch?v=sgz5dutPF8M)
+ [S'inscrire automatiquement à des concours sur Twitter](https://www.youtube.com/watch?v=iAOOdYsK7MM&t=928s)
+ Archiver une page web dans Zotero
+ Créer une base de données pour réaliser des traitements statistiques

## Quand faut-il faire du webscraping ?

Le webscraping n'est qu'une méthode parmi d'autres pour atteindre cet objectif.
Voici une brève description des avantages et inconvénients de ces différentes
approches. Évidemment, le webscraping est presque toujours une option,
à l'inverse des deux autres.

| Méthode | Avantages | Inconvénients |
| :----: | ---- | ---- |
| Webscraping | <ul><li>Furtif (si bien fait)</li><li>Utilisation de l'interface du site</li><li>Contrôle total sur la production de données</li><li>Ludique si on aime hacker</li></ul> | <ul><li>Potentiellement illégal</li><li>Difficultés avec interfaces complexes</li><li>Contrainte de s'adapter aux redesigns</li></ul> |
| API<br />(si proposé par le site) | <ul><li>Légal selon les CGU</li><li>Données dissociées de leur mise en forme</li><li>Requêtes simplifiées</li><li>Durabilité des API</li></ul> | <ul><li>Limites et prix (débit, type de données)</li><li>Processus officiel et déclaré</li><li>Détaché de l'usage d'une interface</li></ul> |
| Accès direct à la base de données | <ul><li>Légal selon l'accord passé</li><li>Informations déjà mises en formes</li><li>Pas de programmation nécessaire !</li></ul> | <ul><li>Négociation d'un accord (temps, limites)</li><li>Pas de contexte de production</li><li>Détaché de l'usage d'une interface</li></ul> |

À vous de voir, selon le projet, quelle méthode est la plus adaptée !

# Comment ça marche ?

Le webscraping tire parti d'**un principe simple : les pages web sont générées
par un processus automatique et déterministe**, en réutilisant la même
structure et la même mise en forme pour afficher différentes informations.

Par conséquent, si nous comprenons ce processus automatique et déterministe,
alors **nous pouvons automatiser la récupération de ces informations**.

Deux corollaires :

0. Moins le processus de génération d'un site est déterministe (écriture
   manuelle, génération aléatoire de contenu comme dans les pages
   personnalisées ou la vente de billets d'avion), moins le webscraping est
   efficace.
1. Pour écrire un bon webscraper, il faut comprendre un minimum d'éléments sur
   la production de sites web.

## Oui, mais comment ça marche ?

En pratique, notre bot va procéder en deux temps :

0. **Le scraper** à proprement parler s'occupe d'extraire les informations sur
   un certain type de page (email d'une archive, liste de tweets, page d'un
   topic sur un forum)
1. **Le crawler** est en charge de naviguer de lien en lien pour pointer le
   scraper vers toutes les pages pertinentes (listes des emails dans un thread,
   liste de profils, listes de threads d'un forum)

Dans un premier temps, on peut se représenter un bot de webscraping comme un
robot qui naviguerait sur un site très très rapidement, comme si nous le
faisions avec notre navigateur. Dans un deuxième temps, nous verrons qu'il peut
être utile de tirer profit du fait que notre robot ne "voit" pas exactement ce
que nous voyons en navigant sur un site. Enfin, dans un troisième temps, nous
verrons que dans certains cas, il peut être souhaitable que notre robot se
comporte de manière moins "robotique".

## Quelles sont les grandes étapes ?

Quelques étapes (non chronologiques) pour se guider dans la création d'un
webscraper :

0. **Se demander quel usage détourné nous voulons faire de ces informations** :
    + Si c'est construire une base de données, alors se poser les questions :
      quels individus (lignes) ? Quelles variables (colonnes) ? Par exemple :
        + Collecter des messages (auteur·e, destinataire, date, contenu, etc.)
        + Collecter des threads (nombre de messages, nom du thread, etc.)
        + Collecter des profils (pseudo, photo de profil, date de création,
          etc.)
1. **Puis commence une phase d'observation** :
    + Étudier la structure du site et de ses pages
    + Déterminer si le site est un bon candidat pour un webscraper
    + Concevoir dans les grandes lignes ce que fera le scraper
2. **Enfin, on passe à l'écriture du scraper**.

Pour l'instant, je propose qu'on se concentre seulement sur les deux premières
étapes.

## Notre projet : botissimo

Nous allons réaliser un bot (*botissimo* :fireworks: ) qui récupère les messages échangés sur les forums
de Doctissimo.

![Forum contraception de Doctissimo](./imgs/forum.png)

# Décortiquer une page web

## Un outil d'exploration essentiel : la console web

La plupart des navigateurs web proposent un outil extrêmement pratique, une
console web, qui permet d'explorer interactivement les différentes composantes
d'une page web.

On peut y accéder facilement :

+ Clic-droit sur une page, il y a souvent une option pertinente dans le menu
  contextuel
+ Dans Firefox : <kbd>Ctrl+Shift+K</kbd>
+ Dans Chromium : <kbd>Ctrl+Shift+I</kbd>

La console présente l'avantage considérable de donner une aide visuelle et
concrète à l'écriture du webscraper, qui lui n'a pas besoin d'afficher ces
informations.

Dans la suite des points présentés, je vous propose d'explorer les éléments
discutés en même temps, via la console de votre navigateur.

Pour simplifier, je vais présenter deux types de technologies qui s'entremêlent
lorsqu'on accède à un site : celles qui permettent d'afficher la page dans
votre navigateur, et celles qui permettent de les manipuler.

## Afficher une page web

Trois principaux éléments permettent d'afficher une page web dans nos
navigateurs.

+ **Le texte de la page lui-même**.

+ **HTML (HyperText Markup Language)** : c'est un langage dit « de balisage »
  (*markup language*), à différencier d'un langage de programmation en ce qu'il
  fournit des informations mais ne donne pas d'instructions directement
  à l'ordinateur. Pour faire vite, il permet d'annoter un texte avec des
  informations supplémentaires qui ne font pas partie du texte. L'usage
  canonique du HTML est de décrire la structure d'une page (telle partie est
  l'en-tête, telle autre est un paragraphe) ou bien sa sémantique (mettre
  l'emphase sur une partie du texte, préciser que telle phrase est une
  citation). En pratique, le HTML est aussi parfois utilisé pour la mise en
  page et la décoration des pages, le rôle principal de CSS.

+ **CSS (Cascading StyleSheets)** : sert à la mise en page. Placer un élément
  de la page à droite d'un autre, adapter une page à la résolution de l'écran,
  changer la couleur d'un lien cliqué, etc.

Nous nous servirons principalement du HTML et du CSS d'une page pour
sélectionner des éléments et extraire leur contenu textuel.

## Manipuler une page web

Un certain nombre de technologies permettent de manipuler ces pages web pour
interagir avec les internautes. Cela implique souvent une communication avec le
serveur (un serveur est simplement un ordinateur dont la fonction est de nous
"servir" des pages web).

+ **HTTP (HyperText Transfer Protocol)** : en gros, c'est un protocole de
  communication qui permet de faire des requêtes pour envoyer ou recevoir de
  l'information. Par exemple, "voir une page" effectue une requête HTTP de type
  `GET`. Remplir un formulaire de contact enverra une requête `POST` au serveur
  avec les informations renseignées.

+ **Javascript** : langage de programmation (différent d'un langage de balisage
  en ce qu'il donne des instructions à exécuter à l'ordinateur) qui permet
  d'introduire un aspect dynamique directement dans le navigateur.  Quelques
  usages courants incluent masquer des éléments d'une page, ouvrir et fermer
  les "fenêtres modales" qui nous demandent de bien vouloir désactiver adblock,
  charger les tweets suivants dans un "infinite scroll", faire des requêtes en
  arrière-plan, etc.

Ces technologies introduisent des éléments plus avancés de webscraping, nous
les laisserons donc pour une prochaine fois.

# Comment sélectionner des éléments d'une page web ?

Nous allons maintenant voir les éléments de base qui nous permettront de
sélectionner les informations d'une page.

## Les balises HTML

Comment se manifestent en pratique les balises HTML ? Imaginons que nous
voulions annoter le texte suivant :

```html
Hello world!
```

On va mettre en gras le dernier mot :

```html
Hello <b>world!</b>
```

Comme on le voit, la plupart des balises HTML comportent une balise ouvrante et
une balise fermante. À l'intérieur des chevrons, on trouve un mot-clé qui
indique plus ou moins clairement le rôle de la balise (ici, b pour bold).

## Une arborescence de balises

Le fait qu'une balise soit en fait constituée d'une ouvrante et d'une fermante
a une conséquence importante pour nous : on peut (et on doit, selon le standard
HTML) imbriquer ces différentes balises entre elles.

Par exemple, je peux faire de cette phrase un paragraphe entier :

```html
<p>Hello <b>world!</b></p>
```

Imaginons maintenant que je veuille sélectionner le mot "world!" : je veux en
fait accéder au contenu de la balise `<b>` dans la balise `<p>`. Selon le
langage qu'on utilise, cette requête s'exprimera de façon différente. Dans le
langage de requête XPath, on peut l'écrire de la sorte : `/p/b/text()`.

Ce qu'il faut surtout retenir, c'est que HTML s'organise dans une structure
arborescente, et qu'on peut en sélectionner des éléments en énonçant leur
chemin. C'est exactement comme l'arborescence de fichiers et de dossiers sur
nos disques durs.

Pour filer la métaphore de l'arbre et de la théorie des graphes, on appelle
parfois les éléments du HTML des nœuds (*nodes*).

## Les attributs

HTML permet aussi d'annoter les balises elles-mêmes avec des informations
contextuelles supplémentaires. C'est ce qu'on appelle des attributs. Ils se
trouvent toujours dans la balise ouvrante. Par exemple, ajoutons un identifiant
à notre paragraphe :

```html
<p id="paragraphe1">Hello <b>world!</b></p>
```

Évidemment, nous pourrions nous servir de ces attributs pour sélectionner des
éléments HTML précis.

## Les classes CSS

Un attribut particulier, qui revient très souvent, est la classe d'un élément.
La classe est ce qui fait le lien entre HTML et le CSS, elle dit au navigateur
d'appliquer une mise en forme décrite dans le CSS à cet élément HTML.

```html
<p id="paragraphe1" class="joli-paragraphe">Hello <b>world!</b></p>
```

J'ai ici appliqué la classe `joli-paragraphe` au paragraphe.

## Le but du jeu

Scraper une page, ce n'est rien de plus que de **trouver un chemin et des
attributs qui mènent uniquement et systématiquement aux éléments qu'on veut
sélectionner**.

# Faire de notre scraper une fonction

Tous les langages de programmation fournissent le concept de fonction. Une
fonction est simplement un petit module de code que l'on peut réutiliser autant
de fois que l'on veut. Il est utile de penser à sa fonction en terme d'`input`
et `output`.

En `input`, nous donnons à la fonction les informations dont elle a besoin pour
fonctionner. Ces informations sont souvent appelées arguments. Par exemple,
notre fonction scraper aura besoin pour fonctionner de l'URL (l'adresse unique)
de la page dont on veut extraire les informations.

En `output`, la fonction peut soit nous donner les résultats de son opération,
soit effectuer une autre opération qui va affecter d'autres parties du
programme. Ici, notre fonction scraper peut soit nous donner les éléments
extraits de la page, soit directement les ajouter à une base de données.

On peut le noter de manière succincte (qui ne correspond à aucun langage
existant) :

```
scraper(url_de_la_page) -> elements
```

# Comment naviguer dans un site web ?

Maintenant que nous savons récupérer des éléments d'une page web, nous voulons
que notre bot puisse naviguer automatiquement sur le site, aller de page en
page et déclencher le scraper déjà établi. Nous sommes donc à l'étape de
réalisation d'un crawler.

Un crawler est finalement un scraper d'un type particulier : il ne va extraire
que les liens présents sur une page. Puis il va traiter ces liens de deux
façons différentes :

1. Si le lien est celui d'une page "cible", il va lancer le scraper sur
   l'adresse de cette page
2. Si le lien mène à une page qui contient d'autres liens cibles, alors il va
   lancer le crawler sur l'adresse de cette page.

En pseudo-code :

```
crawler(url) ->
  si page cible, alors scraper(url)
  si liste de liens, alors crawler(url)
  sinon, ne rien faire
```

## Comment repérer des liens sur une page ?

Nous pouvons repérer des liens sur une page à l'aide d'une balise HTML très
importante, appelée `a` (comme "anchor", ou ancre). Voici comment elle se
présente sous sa forme la plus simple :

```html
<a href="https://duckduckgo.com">Une alternative à Google</a>
```

Ce qui donnera le lien suivant : [Une alternative à Google](https://duckduckgo.com)

Nous voudrons donc récupérer la valeur de l'attribut `href` (pour "Hypertext
Reference") des balises `<a>`.

# Quels outils pour faire du webscraping ?

En réalité, on peut faire du webscraping avec la plupart des logiciels de
programmation. Il existe même des extensions de navigateurs qui donnent accès
à des fonctionnalités de webscraping.

Dans la suite de la formation, nous verrons comment utiliser R pour faire du
webscraping. R est un choix judicieux si l'on veut ensuite analyser les données
extraites.

Python est aussi un outil fréquemment utilisé pour le webscraping. C'est un
langage simple à apprendre, mais qui peut s'avérer très puissant. On peut
également l'employer pour faire de l'analyse de données, même si ce langage est
moins employé en sciences sociales.

Enfin, si vous êtes sur un système UNIX (Linux, macOS, BSD), il est tout à fait
possible de faire un webscraper en Shell.

# Aller plus loin

Pour référence, voici une liste de problèmes et de fonctionnalités
intéressantes, mais qui sortent du cadre d'une introduction au webscraping :

| Problème | Solutions |
| ---- | ---- |
| Des éléments hétérogènes sont mélangés dans du texte brut (une date et un pseudo par exemple) | **Regex** (expressions régulières) |
| Le site requiert une authentification de l'internaute | **Requêtes `POST`** <br /> **Utilisation de cookies** |
| Le site charge des données uniquement par javascript (*infinite scroll*, commentaires YouTube) | **Requêtes POST** <br /> Simulation d'événements javascript |
| Le site limite les informations affichées, par exemple les résultats d'une recherche | Génération artificielles d'URLs |
| Le serveur freine / bannit notre IP| **Délai aléatoire** entre les requêtes <br /> **Changer régulièrement d'adresse IP** (réseau Tor, proxies, VPN)|
| Le site interdit le scraping par des bots | **Modifier le `USERAGENT`** pour passer pour un navigateur standard |
| On veut s'assurer qu'en modifiant notre scraper, les informations récupérées sont toujours correctes | **Tests automatisés** |

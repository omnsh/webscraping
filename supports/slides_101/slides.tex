% chktex-file 26 % enable spaces around punctuation

\documentclass{beamer}

\input{../../.setup/setup_beamer.tex}

\definecolor{lightgray}{HTML}{565656}

\title{Le moissonnage des données en ligne}
\subtitle{Introduction au webscraping}
\date{12 septembre 2019{\color{lightgray}, v.~\gitcommithash}}
\author{Gabriel Alcaras
· \href{mailto:gabriel.alcaras@ehess.fr}{gabriel.alcaras@ehess.fr}}
\institute{Journées du CMH · Fréjus}

\begin{document}
  \maketitle

  \begin{frame}[t]{Une page de publicité\ldots}
    \small
    Séminaire «~Administrer la preuve statistique en sciences sociales~» :\\[3em]

    \begin{columns}
      \begin{column}{0.5\linewidth}
        \begin{figure}
          \includegraphics[width=\textwidth]{figures/xkcd.png}
        \end{figure}
      \end{column}
      \begin{column}{0.5\linewidth}
        Avec Pierre Blavier, Samuel Coavoux, Anton Perdoncin.\bigskip

        Espace de réflexion et de formation collective sur les problèmes
        contemporains de l'inférence statistique.\bigskip

        Crise de la réplication, mise en cause du NHST, ACM, modèles
        bayésiens.\bigskip

        Lundi 7 octobre, 17h-19h, R2-03. 
     \end{column}
    \end{columns}
  \end{frame}

  \section{Introduction}

  \begin{frame}[c]{Vous avez dit «~scraping~» ?}
    Le scraping, ou moissonnage des données, consiste à extraire des
    informations d'une page web pour en faire un usage détourné. \\[2em]

    La question est donc : quels usages ? Ceux de la recherche :

    \begin{itemize}
      \item Récupérer, compléter des données
      \item Usages des plateformes, des réseaux
    \end{itemize}

    \ldots Parmi tous les autres !

  \end{frame}

  \begin{frame}[c]{Objectifs de l'atelier}
    \begin{itemize}
      \item Une introduction aux grands principes
      \item Exploration de pages avec un navigateur
      \item Sans implémentation technique
      \item Donner le vocabulaire et les ressources
    \end{itemize}
  \end{frame}

  \begin{frame}[c]{Ressources}
    Quelques tutoriels :
    \begin{itemize}
      \item 2012, Hobeika \& Ollion, «~Et pourtant, ils mentent~»
        \url{https://quanti.hypotheses.org/724/}
      \item 2018, Larmarange,
        \url{https://larmarange.github.io/analyse-R/scraping.html}
      \item 2018, Roquebert, «~Constituer un corpus Europresse~»
        \url{https://rpubs.com/CorentinRoquebert/europresse}
      \item 2019, Alcaras et Rochat, «~Introduction au webscraping~»,
        \url{https://gitlab.com/omnsh/webscraping}
    \end{itemize}

    Autres ressources : liste quanti ou R-Soc, StackOverflow
  \end{frame}

  \begin{frame}[c]{To scrap or not to scrap?}
    \begin{table}[]
      \begin{tabular}{@{}p{2cm}p{4cm}p{4cm}@{}}
      \toprule
       Méthode & Avantages & Inconvénients  \\ \midrule
       Webscraping & Disponible · Furtif · Interface · Quantification
                   & Légalité · Complexité · Adaptation \\[0.8em]
       API & Légal · Données nettoyées · Facilité · Stabilité & Limites · Prix
       · Perspective développeur \\[2em]
       Accès direct & Légal · Tableau · Gain de temps & Négociation · Seconde main  \\ \bottomrule
      \end{tabular}
    \end{table} 

    Exemples : Sidonie Naulin ; Algopol ; Marie Bergström.
  \end{frame}

  \section{Le scraping dans ses grandes lignes}

  \begin{frame}[c]{Comment ça marche ?}
    Le webscraping tire parti d'\textbf{un principe simple : les pages web sont générées
    par un processus automatique et déterministe}.

    Par conséquent, si nous comprenons ce processus automatique et déterministe,
    alors nous pouvons automatiser la récupération de ces informations.

    Deux corollaires :

    \begin{itemize}
      \item Plus le processus est imprévisible, moins le webscraping est efficace
      \item Pour scraper, il faut comprendre un minimum d'éléments techniques
    \end{itemize}
  \end{frame}

  \begin{frame}[c]{Les grandes étapes}
    Deux modules :

    \begin{itemize}
      \item \textbf{le scraper} : extraire les informations sur un type de pages
      \item \textbf{le crawler} : naviguer de lien en lien pour pointer le
        scraper vers les pages pertinentes
    \end{itemize}

    Comprendre le scraping :

    \begin{itemize}
      \item Pour se le représenter : imite un humain très rapide
      \item Pour l'implémenter : tirer parti du caractère robotique, \textit{bot}
    \end{itemize}
  \end{frame}

  \section{Décortiquer une page web}

  \begin{frame}[c]{L'outil essentiel : la console web}
    La console web permet d'explorer interactivement les différentes composantes
    d'une page web.\\[2em]

    On peut y accéder facilement :

    \begin{itemize}
      \item Clic-droit sur une page, il y a souvent une option pertinente dans
        le menu contextuel
      \item Dans Firefox : \texttt{Ctrl+Shift+K}
      \item Dans Chromium : \texttt{Ctrl+Shift+I}
    \end{itemize}
  \end{frame}

  \begin{frame}[c]{Naviguer sur un site}
    Afficher une page web :

    \begin{itemize}
      \item Le texte de la page lui-même
      \item HTML (HyperText Markup Language) : structure du texte
      \item CSS (Cascading StyleSheets) : mise en page
    \end{itemize}

    \bigskip

    Interagir avec une page web :

    \begin{itemize}
      \item HTTP (HyperText Transfer Protocol) : requêtes avec le serveur
      \item Javascript : aspect dynamique directement dans le navigateur
    \end{itemize}
  \end{frame}

  \section{Extraire des éléments d'une page}

  \begin{frame}[c]{Sélectionner des éléments sur une page}
    Un texte : \texttt{Hello world!}

    Un texte annoté (gras) : \texttt{Hello <b>world!</b>}

    Arborescence : \texttt{<p>Hello <b>world!</b></p>}

    Attributs HTML :

    \texttt{<p id="paragraphe1">Hello <b>world!</b></p>}

    Classes CSS :

    \texttt{<p id="paragraphe1" class="joli-paragraphe">Hello <b>world!</b></p>}
  \end{frame}

  \begin{frame}[c]{Faire le crawler}
  Un crawler est un scraper d'un type particulier, qui ne s'occupe que des
  liens :

  \begin{itemize}
    \item  Si le lien est celui d'une page "cible", il va lancer le scraper sur
     l'adresse de cette page
     \item Si le lien mène à une page qui contient d'autres liens cibles, alors il va
     lancer le crawler sur l'adresse de cette page.
  \end{itemize}

  \bigskip

  En HTML :

  \texttt{<a href="https://duckduckgo.com">Une alternative à Google</a>}
  \end{frame}

  \section{Aller plus loin}

  \begin{frame}[c]{Quels outils ?}
    \begin{itemize}
      \item Macros (Firefox, Chrome, etc.)
      \item R
      \item Selenium
      \item Python
    \end{itemize}
  \end{frame}

  \begin{frame}[c]{Difficultés récurrentes}
    Quelques problèmes récurrents et des pointeurs vers des solutions :

    \begin{table}[]
      \begin{tabular}{@{}p{5cm}p{4cm}@{}}
      \toprule
        Problème & Solutions \\ \midrule
        Filtrer du texte & Expressions régulières \\
        Authentification & \texttt{POST}, cookies \\
        Javascript & \texttt{POST}, simulation \\
        Masquage des résultats & \texttt{URLs} \\
        Bannissement & Délai, changement d'IP , \texttt{USERAGENT} \\
       \bottomrule
      \end{tabular}
    \end{table} 
  \end{frame}

  \begin{frame}[c]{Ressources}
    Quelques tutoriels :
    \begin{itemize}
      \item 2012, Hobeika \& Ollion, «~Et pourtant, ils mentent~»
        \url{https://quanti.hypotheses.org/724/}
      \item 2018, Larmarange,
        \url{https://larmarange.github.io/analyse-R/scraping.html}
      \item 2018, Roquebert, «~Constituer un corpus Europresse~»
        \url{https://rpubs.com/CorentinRoquebert/europresse}
      \item 2019, Alcaras et Rochat, «~Introduction au webscraping~»,
        \url{https://gitlab.com/omnsh/webscraping}
    \end{itemize}
  \end{frame}

\end{document}
